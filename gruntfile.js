module.exports = function (grunt) {

    var package = grunt.file.readJSON('package.json');

    var scss_directories = [];

    scss_directories.push(package.scss_directory + "/{**/*,*}.{scss,css}");

    grunt.initConfig({

        /**
         * Scripts
         */

        //Build native js
        babel: {
            options: {
                plugins: ['transform-react-jsx'],
                presets: ['es2015', 'react']
            },
            jsx: {
                files: [{
                    expand: true,
                    cwd: 'js/src/core/',
                    src: ['{**/*,*}.jsx'],
                    dest: 'js/build/core/',
                    ext: '.js'
                },{
                    expand: true,
                    cwd: 'js/src/components/',
                    src: ['{**/*,*}.jsx'],
                    dest: 'js/build/components/',
                    ext: '.js'
                },{
                    expand: true,
                    cwd: 'js/src/widgets/grid/',
                    src: ['{**/*,*}.jsx'],
                    dest: 'js/build/widgets/grid/',
                    ext: '.js'
                }]
            }
        },

        //Concat js files
        concat: {
            grid: {
                options: {
                    separator: ''
                },
                /* Watch out: INHERITANCE ORDER */
                src: ["js/build/core/*.js",
                    "js/build/components/Icon.js",
                    "js/build/components/OrderIcon.js",
                    "js/build/components/Link.js",
                    "js/build/components/Button.js",
                    "js/build/components/PaginationButton.js",
                    "js/build/components/ClickableColumnHeader.js",
                    "js/build/widgets/grid/gridRow.js",
                    "js/build/widgets/grid/grid.js",
                    "js/build/widgets/grid/index.js"
                ],
                dest: 'js/concat/gridWidget.js'
            }
        },


        // Minify JS
        uglify: {
            main: {
                options: {
                    beautify: package.devMode
                },
                files: [
                    {
                        expand: true,
                        cwd: 'js/concat/',
                        src: '*.js',
                        dest: 'js/dist/',
                        rename: function (dest, matchedSrcPath, options) {
                            // return the destination path and filename:
                            return (dest + matchedSrcPath).replace('.js', '.min.js');
                        }
                    }
                ]
            }
        },


        /**
         * Styles
         */

        // Compile SCSS into CSS
        sass: {
            main: {
                files: {
                    'css/build/main.css': 'css/sass/main.scss'
                }
            }
        },

        // Clean map files
        clean: {
            scss: [
                'css/build/main.css.map'
            ]
        },

        // Minify CSS
        cssmin: {
            main: {
                options: {
                    shorthandCompacting: false,
                    roundingPrecision: -1
                },
                files: [
                    {
                        expand: true,
                        cwd: 'css/build/',
                        src: '*.css',
                        dest: 'css/dist/',
                        rename: function (dest, matchedSrcPath, options) {
                            // return the destination path and filename:
                            return (dest + matchedSrcPath).replace('.css', '.min.css');
                        }
                    }
                ]
            }
        },

        // No minify on css file if it's dev environment
        copy: {
            main: {
                src: 'css/build/main.css',
                dest: 'css/dist/main.min.css'
            },
        },


        /**
         * Watch
         */

        // watch changes
        watch: {
            js: {
                files: [
                    'js/src/core/*.jsx',
                    'js/src/core/{**/*,*}*.jsx',
                    'js/src/components/*.jsx',
                    'js/src/components/{**/*,*}*.jsx',
                    'js/src/widgets/{**/*,*}*.jsx'
                ],
                tasks: ['babel', 'concat', 'uglify']
            },
            css: {
                files: [
                    'css/sass/*/scss'
                ],
                tasks: ['sass:main', ((package.devMode === true) ? 'copy:main' : 'cssmin:main'), 'clean']
            }
        }


    });

    //register script operations
    grunt.loadNpmTasks('grunt-babel');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');

    //register stylesheet operations
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');

    //register watching
    grunt.loadNpmTasks('grunt-contrib-watch');


    //start
    if (package.devMode === true) {
        grunt.registerTask('default', ['babel', 'concat', 'uglify', 'sass', 'copy', 'clean', 'watch:js']);
    } else {
        grunt.registerTask('default', ['babel', 'concat', 'uglify', 'sass', 'cssmin', 'clean']);
    }
}
