'use strict';
app.Style = {

    defaultButtonClass: function () {
        return 'btn btn-primary';
    },

    defaultLinkClass: function() {
        return 'btn btn-primary'
    },

    defaultColumnHeaderClass: function() {
        return ''
    },

    clickableColumnHeaderClass: function() {
        return 'mouse-pointer'
    },

    defaultTableClass: function () {
        return 'table table-striped';
    },

    defaultOrderDirectionIcon: function (order) {
        if (order === 'desc') {
            return 'glyphicon glyphicon-menu-down';
        } else if( order === 'asc') {
            return 'glyphicon glyphicon-menu-up'
        } else {
            return '';
        }
    }

}
