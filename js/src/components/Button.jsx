'use strict';
class Button extends  Link {

    render() {

        return (

            <button className={((this.props.class) ? this.props.class : app.Style.defaultButtonClass())}
                    type="button"
                    onClick={this.click}>{this.props.text}</button>

        );
    }
}
