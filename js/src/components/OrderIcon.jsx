'use strict';
class OrderIcon extends Icon {

    render() {

        return (

            <span>{this.props.text} <i className={this.props.class}></i></span>

        );
    }
}