'use strict';
class Link extends React.Component {

    constructor(props) {
        super(props);
        this.click = this.click.bind(this);
    }

    click() {
        this.props.onClick(this);
    }

    render() {

        return (

            <a className={((this.props.class) ? this.props.class : app.Style.defaultLinkClass())}
                    onClick={this.click}>{this.props.text}</a>

        );
    }
}
