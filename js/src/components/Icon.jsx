'use strict';
class Icon extends React.Component {

    constructor(props) {
        super(props);
    }

    render() {

        return (

            <i className={this.props.class}></i>

        );
    }
}
