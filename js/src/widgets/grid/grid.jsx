'use strict';
class ExampleGridWidget extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            rows: [],
            order: (props.order && typeof props.order != 'undefined') ? props.order : {"name" : '', "direction" : ''},
            offset: 0,
            limit: 10,
            count: 0
        };

        this.handleOrder = this.handleOrder.bind(this);
        this.handleOffset = this.handleOffset.bind(this);
        this.handlePage = this.handlePage.bind(this);
    }


    componentDidMount() {
        this.setState({"rows" : []});
        this.loadData();
    }


    loadData() {
        var grid = this;
        app.Util.getJson(this.props.dataSource, function(data) {
            var rows = [];

            if (grid.state.order.name) {
                data = app.Util.orderBy(grid.state.order.name, grid.state.order.direction, data);
            }

            grid.setState({"count": data.length});

            data = data.slice(grid.state.offset, (grid.state.limit + grid.state.offset));

            data.forEach(function(item) {
                rows.push(<ExampleGridRow item={item} key={item.id} />);
            });

            grid.setState({"rows": rows});
        });
    }


    toggleOrder(e) {
        var order = this.state.order;
        if (e.props.name == this.state.order.name) {
            if (this.state.order.direction == 'asc') {
                order = 'desc';
            } else if (this.state.order.direction == 'desc') {
                order = 'asc';
            } else {
                order = 'asc';
            }
        } else {
            order = 'asc';
        }
        return order;
    }


    handleOrder(e) {
        this.setState({"order" : {"name" : e.props.name, "direction": this.toggleOrder(e)}});
        this.setState({"offset" : 0});
        this.loadData();
    }


    handleOffset(e) {
        var offset = this.state.offset;
        offset = offset + (this.state.limit * e.props['data-page']);
        offset = (offset > 0) ? ((offset < this.state.count - this.state.limit) ? offset : (this.state.count - this.state.limit)) : 0;
        this.setState({"offset": offset});
        this.loadData();
    }

    handlePage(e) {
        var page = e.props['data-page'];
        var offset = 0;
        if (page == -1) {
            //last page
            offset = ((this.state.count - this.state.limit) > 0) ? this.state.count - this.state.limit : 0;
        }
        this.setState({"offset": offset});
        this.loadData();
    }

    render() {
        return (
            <div>
                <table className={app.Style.defaultTableClass()}>
                    <thead>
                    <tr>
                        <ClickableColumnHeader text={((this.state.order.name == 'id') ? <OrderIcon text="Id" class={app.Style.defaultOrderDirectionIcon(this.state.order.direction)}></OrderIcon> : 'Id')}
                                           name="id"
                                           onClick={this.handleOrder} />
                        <ClickableColumnHeader text={ ((this.state.order.name == 'first_name') ? <OrderIcon text="First Name" class={app.Style.defaultOrderDirectionIcon(this.state.order.direction)}></OrderIcon> : 'First Name')}
                                           name="first_name"
                                           onClick={this.handleOrder} />
                        <ClickableColumnHeader text={((this.state.order.name == 'last_name') ?  <OrderIcon text="Last Name" class={app.Style.defaultOrderDirectionIcon(this.state.order.direction)}></OrderIcon> : 'Last Name')}
                                           name="last_name"
                                           onClick={this.handleOrder} />
                        <ClickableColumnHeader text={((this.state.order.name == 'email') ?  <OrderIcon text="Email" class={app.Style.defaultOrderDirectionIcon(this.state.order.direction)}></OrderIcon> : 'Email')}
                                           name="email"
                                           onClick={this.handleOrder} />
                        <ClickableColumnHeader text={((this.state.order.name == 'gender') ? <OrderIcon text="Gender" class={app.Style.defaultOrderDirectionIcon(this.state.order.direction)}></OrderIcon> : 'Gender')}
                                           name="gender"
                                           onClick={this.handleOrder} />
                        <ClickableColumnHeader text={((this.state.order.name == 'ip_address') ? <OrderIcon text="IP Address" class={app.Style.defaultOrderDirectionIcon(this.state.order.direction)}></OrderIcon> : 'IP Address')}
                                           name="ip_address"
                                           onClick={(this.state.order.name =='ip_address') ? this.state.order.direction : ''} />
                    </tr>
                    </thead>
                    <tbody>{this.state.rows}</tbody>
                </table>
                <PaginationButton text="<< First" onClick={this.handlePage} data-page="0"/> <PaginationButton text="< Prev" onClick={this.handleOffset} data-page="-1"/> <PaginationButton text="Next >" onClick={this.handleOffset} data-page="1"/> <PaginationButton text="Last >>" onClick={this.handlePage} data-page="-1"/>
            </div>
        );
    }
}
