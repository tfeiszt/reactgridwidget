'use strict';
class ExampleGridRow extends React.Component {
    render() {
        return (

            <tr>
                <td>{this.props.item.id}</td>
                <td>{this.props.item.first_name}</td>
                <td>{this.props.item.last_name}</td>
                <td>{this.props.item.email}</td>
                <td>{this.props.item.gender}</td>
                <td>{this.props.item.ip_address}</td>
            </tr>

        );
    }
}
