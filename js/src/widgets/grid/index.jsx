'use strict';
app.Bootstrap = app.Bootstrap  || {}

app.Bootstrap.showGridWidget =  function(dataFile, defaultOrder){

    var order = (defaultOrder && typeof defaultOrder != 'undefined') ? defaultOrder : {'name': "id", "direction": "asc"}

    ReactDOM.render(
        <ExampleGridWidget dataSource={dataFile} order={order} />, document.getElementById('widget-container')
    );

}
