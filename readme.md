# tfeiszt ReactGridWidget  

### Description
React grid widget template with ordering and pagination.
 
### Browser compatibility
   * IE9
   * IE10
   * IE11
   * Firefox
   * Safari
   * Opera
   * Chrome
   * Edge

### Pre-processors
  * Babel
  * Sass
  
### Compiler
  * Grunt
  
### Installing  

```
$ npm install
$ bower install
$ grunt
```
  
### Grunt setting to build production
See option "devMode" in package.json. If this option is false then css and js files are minimized, otherwise not. Default option is true.
```
{
    "name": "react-grid",
    "descrition": "React grid example",
    "version": "0.0.1",
    "devMode": false,
```
 
### License

MIT
